import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from myUi import *
import re


class Register:
    def __init__(self, name):
        self.name = name

        self.high_byte = bytes(1)
        self.low_byte = bytes(1)

    def move_into(self, wartosc, czesc, is_int=False):
        """
        Domyslnie wartosc to byte, jesli nie to uznaje ze to int mieszczacy sie na jednym bajcie!
        Jak sie niem miesci bedzie błąd !
        wartosc = wartosc.to_bytes(1, byteorder='big')
        (musi byc wtedy mniejsza rowna 255 !)
        """
        if is_int:
            wartosc = wartosc.to_bytes(1, byteorder='big')

        if czesc:
            # czesc starsza
            self.high_byte = wartosc
        else:
            # czesc mlodsza
            self.low_byte = wartosc

    def add(self, wartosc, czesc, is_int=True):
        """
         Domyslnie wartosc to int, jesli nie to uznaje ze to byte
        """
        if not is_int:
            wartosc = int.from_bytes(wartosc, byteorder='big')
        if czesc:
            # czesc starsza
            reg_this = int.from_bytes(self.high_byte, byteorder='big')
        else:
            # czesc mlodsza
            reg_this = int.from_bytes(self.low_byte, byteorder='big')

        reg_this += wartosc
        if reg_this > 255:
            reg_this = reg_this%255

        if czesc:
            # czesc starsza
            self.high_byte = reg_this.to_bytes(1, byteorder='big')
        else:
            # czesc mlodsza
            self.low_byte = reg_this.to_bytes(1, byteorder='big')

    def sub(self, wartosc, czesc, is_int=True):
        """
        Domyslnie wartosc to int, jesli nie to uznaje ze to byte
        """
        if not is_int:
            wartosc = int.from_bytes(wartosc, byteorder='big')

        if czesc:
            # czesc starsza
            reg_this = int.from_bytes(self.high_byte, byteorder='big')
        else:
            # czesc mlodsza
            reg_this = int.from_bytes(self.low_byte, byteorder='big')

        reg_this -= wartosc
        if reg_this < 0:
            # Przypadek gdy wartosc jest mniejsza od 0
            reg_this = abs(reg_this)
        if czesc:
            # czesc starsza
            self.high_byte = reg_this.to_bytes(1, byteorder='big')
        else:
            # czesc mlodsza
            self.low_byte = reg_this.to_bytes(1, byteorder='big')


    def to_int(self, czesc):
        if czesc:
            # czesc starsza
            return int.from_bytes(self.high_byte, byteorder='big')
        else:
            # czesc mlodsza
            return int.from_bytes(self.low_byte, byteorder='big')

    def take_byte(self, czesc):
        if czesc:
            # czesc starsza
            return self.high_byte
        else:
            # czesc mlodsza
            return self.low_byte


class Microprocesor_cal:
    def __init__(self):
        AX, BX, CX, DX = Register('AX'), Register('BX'), Register('CX'), Register('DX')
        self.registers = {'AX': AX, 'BX': BX, 'CX': CX, 'DX': DX}

        self.registers_dec = {'AXL': ('AX', 0), 'AXH': ('AX', 1), 'BXL': ('BX', 0), 'BXH': ('BX', 1),
                              'CXL': ('CX', 0), 'CXH': ('CX', 1), 'DXL': ('DX', 0), 'DXH': ('DX', 1)}

    def move_reg(self, reg_1, reg_2):
        reg_1, czesc_1 = self.decode_reg(reg_1)
        if reg_2 in self.registers_dec:
            reg_2, czesc_2 = self.decode_reg(reg_2)
            self.registers[reg_1].move_into(self.registers[reg_2].take_byte(czesc_2), czesc_1)
        else:
            self.registers[reg_1].move_into(int(reg_2), czesc_1, is_int=True)

    def add_reg(self, reg_1, reg_2):
        reg_1, czesc_1 = self.decode_reg(reg_1)
        if reg_2 in self.registers_dec:
            reg_2, czesc_2 = self.decode_reg(reg_2)
            self.registers[reg_1].add(self.registers[reg_2].to_int(czesc_2), czesc_1)
        else:
            self.registers[reg_1].add(int(reg_2), czesc_1)

    def sub_reg(self, reg_1, reg_2):
        reg_1, czesc_1 = self.decode_reg(reg_1)
        if reg_2 in self.registers_dec:
            reg_2, czesc_2 = self.decode_reg(reg_2)
            self.registers[reg_1].sub(self.registers[reg_2].to_int(czesc_2), czesc_1)
        else:
            self.registers[reg_1].sub(int(reg_2), czesc_1)

    def show_register_int(self, reg):
        reg, czesc = self.decode_reg(reg)
        return self.registers[reg].to_int(czesc)

    def show_register_byte(self, reg):
        reg, czesc = self.decode_reg(reg)
        return self.registers[reg].take_byte(czesc)

    def decode_reg(self, reg):
        reg_name, czesc, = self.registers_dec[reg]
        return reg_name, czesc

    def ex_instruction(self, line):
        """
        :param line: lista z rozdzielonymi: nazwa instrukcji  i argumentami
        Wykonanie podanej instrukcji na danych parametrach
        """
        arg_1 = line[1]
        arg_2 = line[2]

        instruction_name = line[0]
        # print(line)
        if instruction_name == 'MOV':
            self.move_reg(arg_1, arg_2)
        elif instruction_name == 'ADD':
            self.add_reg(arg_1, arg_2)
        elif instruction_name == 'SUB':
            self.sub_reg(arg_1, arg_2)
        else:
            print("Podano nieprawidlowa instrukcje")


class Microprocessor(QtWidgets.QMainWindow):

    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.microprocessor = Microprocesor_cal()

        self.split_code = []
        self.line_no = 0


        self.ui.AXH.clicked.connect(self.show_registers_data)
        self.ui.Data.clicked.connect(self.show_registers_data)
        self.ui.AXL.clicked.connect(self.show_registers_data)
        self.ui.BXH.clicked.connect(self.show_registers_data)
        self.ui.BXL.clicked.connect(self.show_registers_data)
        self.ui.CXL.clicked.connect(self.show_registers_data)
        self.ui.CXH.clicked.connect(self.show_registers_data)
        self.ui.DXL.clicked.connect(self.show_registers_data)
        self.ui.DXH.clicked.connect(self.show_registers_data)
        self.ui.AXHo.clicked.connect(self.show_registers_data)
        self.ui.AXLo.clicked.connect(self.show_registers_data)
        self.ui.BXHo.clicked.connect(self.show_registers_data)
        self.ui.BXLo.clicked.connect(self.show_registers_data)
        self.ui.CXLo.clicked.connect(self.show_registers_data)
        self.ui.CXHo.clicked.connect(self.show_registers_data)
        self.ui.DXLo.clicked.connect(self.show_registers_data)
        self.ui.DXHo.clicked.connect(self.show_registers_data)
        self.ui.Data.clicked.connect(self.show_registers_data)

        self.ui.AddStep.clicked.connect(self.add_step)
        self.ui.Execute.clicked.connect(self.execute_all)
        self.ui.LoadFile.clicked.connect(lambda: self.load_from_file("program.txt"))
        self.ui.Save.clicked.connect(lambda: self.save_to_file("program.txt"))
        self.ui.DeleteProgram.clicked.connect(self.delete_program)
        self.ui.DeleteLine.clicked.connect(self.delete_line)
        self.ui.StepByStep.clicked.connect(self.step_by_step_execution)
        self.show()


    def add_step(self):
        first, second = self.register_choosing()
        operation = self.ui.Operation.currentText()
        if self.ui.Data.isChecked() == True and first and operation:
            if self.ui.DataToRegister.text().isnumeric():
                if int(self.ui.DataToRegister.text()) > 255:
                    self.ui.DataToRegister.setText("255")
                elif int(self.ui.DataToRegister.text()) >= 0:
                 self.split_code.append([operation, first,  self.ui.DataToRegister.text()])
            else:
                self.ui.DataToRegister.setText("")
        if first and second and operation:
            self.split_code.append([operation, first, second])
        self.show_program()

    def register_choosing(self):
        first = ''
        second = ''
        if self.ui.AXH.isChecked()==True:
            second = 'AXH'
        elif self.ui.AXL.isChecked()==True:
            second = 'AXL'
        elif self.ui.BXH.isChecked()==True:
            second = 'BXH'
        elif self.ui.BXL.isChecked()==True:
            second = 'BXL'
        elif self.ui.CXL.isChecked()==True:
            second = 'CXL'
        elif self.ui.CXH.isChecked()==True:
            second = 'CXH'
        elif self.ui.DXH.isChecked()==True:
            second = 'DXH'
        elif self.ui.DXL.isChecked()==True:
            second = 'DXL'
        if self.ui.AXHo.isChecked()==True:
            first = 'AXH'
        elif self.ui.AXLo.isChecked()==True:
            first = 'AXL'
        elif self.ui.BXHo.isChecked()==True:
            first = 'BXH'
        elif self.ui.BXLo.isChecked()==True:
            first = 'BXL'
        elif self.ui.CXLo.isChecked()==True:
            first = 'CXL'
        elif self.ui.CXHo.isChecked()==True:
            first = 'CXH'
        elif self.ui.DXHo.isChecked()==True:
            first = 'DXH'
        elif self.ui.DXLo.isChecked()==True:
            first = 'DXL'
        return first, second

    def show_program(self):
        commands = ""
        i = 0
        for line in self.split_code:
            commands = "{}. ".format(i).join((commands, " ".join((line[0],", ".join((line[1], line[2] + "\n"))))))
            i += 1

        self.ui.textBrowser.setText(commands)

    def show_registers_data(self):
        first, second = self.register_choosing()
        if self.ui.Data.isChecked() == True and second:
            self.ui.OutputData.setText(self.ui.DataToRegister.text())
            self.ui.InputData.setText(str(self.microprocessor.show_register_int(second)))
        if first and second:
            self.ui.InputData.setText(str(self.microprocessor.show_register_int(second)))
            self.ui.OutputData.setText(str(self.microprocessor.show_register_int(first)))

    def load_from_file(self, file_name):
        """
        Zaladowanie kodu z pliku o nazwie file_name i zapisanie go do listy split_code (z instrukcjami rozdzielonymi od
        argumentów)
        """
        self.delete_program()
        file = open(file_name, 'r')
        lines = file.readlines()
        for line in lines:
            split_line = re.split(' |, |;|\n', line)
            split_line = split_line[:3]
            self.split_code.append(split_line)
        file.close()
        self.show_program()

    def save_to_file(self, file_name):  #  zostawiam tobie, będziesz wiedzial co zrobic
        file = open(file_name, 'w')
        for line in self.split_code:
            file.write(line[0] + " " + line[1] + ", " + line[2] + "\n")
        file.close()

    def execute_all(self):
        """
        Wykonanie całego kodu
        """
        for line in self.split_code:
            self.microprocessor.ex_instruction(line)
        self.show_registers_data()
        self.show_program()

    def step_by_step_execution(self):


        if self.line_no >= len(self.split_code):
            self.line_no = 0
            self.ui.StepLine.setText(str(self.line_no))
            return
        line = self.split_code[self.line_no]
        self.microprocessor.ex_instruction(line)
        self.line_no += 1
        self.ui.StepLine.setText(str(self.line_no))



        self.show_registers_data()
        self.show_program()

    def delete_program(self):
        self.split_code = []
        self.show_program()

    def delete_line(self):
        if self.ui.LineNo.text().isnumeric():
            deleting_line = int(self.ui.LineNo.text())
            if deleting_line >= 0 and deleting_line <= len(self.split_code):
                self.split_code.pop(deleting_line)
        self.ui.LineNo.setText("")
        self.show_program()




def run():
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = Microprocessor()
    sys.exit(app.exec_())

run()
